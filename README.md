# ics-ans-role-logo

Ansible role to install node-red and control LOGOs (PLCs).

Node-RED is a flow-based development tool for visual programming.

LOGOs are logic modules from Siemens

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-logo
```

## License

BSD 2-clause
