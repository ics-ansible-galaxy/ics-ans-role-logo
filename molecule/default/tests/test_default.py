import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_default(host):
    with host.sudo():
        node_red = host.docker("node-red-logo")
        assert node_red.is_running
